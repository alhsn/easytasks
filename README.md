# EasyTasks

An ASP.NET Core Sample app with DDD approach, Repository Pattern, Unit of Work.

## To update the database use:



```

    Update-Database -Context EasyTasksContext -Project Infra -StartupProject Web
    Update-Database -Context AppIdentityContext -Project Infra -StartupProject Web
    
```

