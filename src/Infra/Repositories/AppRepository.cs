﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Interfaces;
using Core.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Linq;

namespace Infra.Repositories
{
    public class AppRepository<T> : IAppRepository<T> where T : AppEntity
    {
        protected readonly DbContext Context;

        public AppRepository(DbContext dbContext)
        {
            Context = dbContext;
        }

        public bool Any()
        {
            return Context.Set<T>().AnyAsync().Result;
        }

        public void Add(T entity)
        {
            Context.Set<T>().Add(entity);
        }

        public void AddRange(ICollection<T> entities)
        {
            Context.Set<T>().AddRange(entities);
        }

        public T Get(int id)
        {
            return Context.Set<T>().Find(id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Context.Set<T>().ToListAsync();
        }

        public void Remove(T entity)
        {
            Context.Set<T>().Remove(entity);
        }

        public void RemoveRange(ICollection<T> entities)
        {
            Context.Set<T>().RemoveRange(entities);
        }

        public IEnumerable<T> AllWhere(Expression<Func<T, bool>> expression)
        {
            return Context.Set<T>().Where(expression).AsEnumerable();
        }

        public T SingleOrDefault(Expression<Func<T, bool>> expression)
        {
            return Context.Set<T>().SingleOrDefault(expression);
        }

        public void Update(T entity)
        {
            Context.Entry<T>(entity).State = EntityState.Modified;
        }
    }
}
