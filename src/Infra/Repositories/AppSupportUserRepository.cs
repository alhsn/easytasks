﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Infra.Data;
using Microsoft.EntityFrameworkCore;

namespace Infra.Repositories
{
    public class AppSupportUserRepository : AppRepository<AppSupportUser>, IAppSupportUserRepository
    {
        public AppSupportUserRepository(EasyTasksContext context) : base(context)
        {
        }

        public async Task<AppSupportUser> GetSupportUserWithAssignedTasks(int id)
        {
            //Include(u => u.AssignedTasks)
            return await EasyTasksContext.Users.SingleOrDefaultAsync(u => u.Id == id);
        }

        public async Task<AppSupportUser> GetSupportUserWithCreatedTasks(int id)
        {
            //.Include(u => u.CreatedTasks)
            return await EasyTasksContext.Users.SingleOrDefaultAsync(u => u.Id == id);
        }

        public EasyTasksContext EasyTasksContext
        {
            get { return Context as EasyTasksContext; }
        }
    }
}
