﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Interfaces;
using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Infra.Data;
using System.Threading.Tasks;
using System.Linq;

namespace Infra.Repositories
{
    public class AppTaskRepository : AppRepository<AppTask>, IAppTaskRepository
    {
        public AppTaskRepository(EasyTasksContext context) : base(context)
        {
        }

        public async Task<IEnumerable<AppTask>> GetAppTasksWithUsers()
        {
            return await EasyTasksContext.Tasks.Include(t => t.AssignedTo).Include(t => t.CreatedBy).ToListAsync();
        }

        public AppTask GetAppTaskWithUsers(int taskId)
        {
            return EasyTasksContext.Tasks
                .Include(t => t.AssignedTo).Include(t => t.CreatedBy)
                .Where(t => t.Id == taskId).FirstOrDefault();
        }

        public AppTask GetAppTaskWithUsers(int taskId, int userId)
        {
            return EasyTasksContext.Tasks
                .Include(t => t.AssignedTo).Include(t => t.CreatedBy)
                .Where(t => t.Id == taskId && (t.CreatedById == userId || t.AssignedToId == userId)).FirstOrDefault();
        }

        public EasyTasksContext EasyTasksContext
        {
            get { return Context as EasyTasksContext; }
        }
    }
}
