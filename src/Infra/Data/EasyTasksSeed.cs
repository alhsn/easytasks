﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Entities;
using Infra.Identity;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Infra.Data
{
    public class EasyTasksSeed
    {
        public static void SeedUsersAndTasks(AppUnitOfWork unitOfWork, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            var users = new List<AppSupportUser>();
            var tasks = new List<AppTask>();


            var user1 = new AppSupportUser { UserName = "Support_Ahmed" };
            var user2 = new AppSupportUser { UserName = "Support_Basim" };
            var user3 = new AppSupportUser { UserName = "Support_Careem" };

            users.Add(user1);
            users.Add(user2);
            users.Add(user3);
            // Add users first to generate IDs
            unitOfWork.UsersRepository.AddRange(users);

            // User 1 own To Do tasks:
            tasks.Add(new AppTask
            {
                CreatedById = user1.Id,
                AssignedToId = user1.Id,
                Title = "Fix Data Seed bug",
                Details = "Refere to bug report #0001",
                Completed = true
            });

            tasks.Add(new AppTask
            {
                CreatedById = user1.Id,
                AssignedToId = user1.Id,
                Title = "Send web log report to Ahmed",
                Details = "Generate full web log report for for Q3"
            });

            // user 1 tasks assigned to user 2
            tasks.Add(new AppTask
            {
                CreatedById = user1.Id,
                AssignedToId = user2.Id,
                Title = "Prepare wireframe desing for the new website",
                Details = "Include desings for: Landing page, News Page, Sign Up, Profile"
            });
            tasks.Add(new AppTask
            {
                CreatedById = user1.Id,
                AssignedToId = user2.Id,
                Title = "Generate A/B test for the new homepage",
                Details = "Duration should be no more than 4 weeks, and target desktop visits only."
            });

            // user 2 tasks assigned to user 1
            tasks.Add(new AppTask
            {
                CreatedById = user2.Id,
                AssignedToId = user1.Id,
                Title = "Prepare Test Server with dummy data",
                Details = "Size of database in Test server need to be similar to Production for Performance Tests."
            });
            tasks.Add(new AppTask
            {
                CreatedById = user2.Id,
                AssignedToId = user1.Id,
                Title = "Write custom parser for products feed file",
                Details = "Convert feed data from XML to csv and flatten nested nodes"
            });
            tasks.Add(new AppTask
            {
                CreatedById = user2.Id,
                AssignedToId = user1.Id,
                Title = "Create products-recommendation engine",
                Details = "For a given product generate: Similar Porducts, Complementary Products, and Most Viewed."
            });

            unitOfWork.TasksRepository.AddRange(tasks);
            unitOfWork.Save();


            //Create Identity roles
            if (! roleManager.RoleExistsAsync("Admin").Result)
            {
                var admin_result = roleManager.CreateAsync(new IdentityRole("Admin")).Result;
                if (!admin_result.Succeeded)
                {
                    throw new Exception("Failed to created Admin Role");
                }
            }

            if (! roleManager.RoleExistsAsync("Support").Result)
            {
                var support_result = roleManager.CreateAsync(new IdentityRole("Support")).Result;
                if (!support_result.Succeeded)
                {
                    throw new Exception("Failed to created Support Role");
                }
            }

            //Create Identity users records
            var support_1 = new ApplicationUser { AppId = user1.Id, UserName = user1.UserName, Email = user1.UserName + "@EasyTasks.Web" };
            var support_2 = new ApplicationUser { AppId = user2.Id, UserName = user2.UserName, Email = user2.UserName + "@EasyTasks.Web" };
            var support_3 = new ApplicationUser { AppId = user3.Id, UserName = user3.UserName, Email = user3.UserName + "@EasyTasks.Web" };

            var admin_1 = new ApplicationUser { UserName = "Admin_1", Email = "Admin_1@EasyTasks.Web" };
            var admin_2 = new ApplicationUser { UserName = "Admin_2", Email = "Admin_2@EasyTasks.Web" };

            var su_result_1 = userManager.CreateAsync(support_1, "Pass@user1").Result;
            var su_result_2 = userManager.CreateAsync(support_2, "Pass@user2").Result;
            var su_result_3 = userManager.CreateAsync(support_3, "Pass@user3").Result;

            var au_result_1 = userManager.CreateAsync(admin_1, "Pass@admin1").Result;
            var au_result_2 = userManager.CreateAsync(admin_2, "Pass@admin2").Result;

            var tmp1 = userManager.AddToRoleAsync(support_1, "Support").Result;
            var tmp2 = userManager.AddToRoleAsync(support_2, "Support").Result;
            var tmp3 = userManager.AddToRoleAsync(support_3, "Support").Result;
            var tmp4 = userManager.AddToRoleAsync(admin_1, "Admin").Result;
            var tmp5 = userManager.AddToRoleAsync(admin_2, "Admin").Result;

            //TODO: check account creation results

        }
    }
}
