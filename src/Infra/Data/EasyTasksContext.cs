﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Infra.EntityConfig;

namespace Infra.Data
{
    public class EasyTasksContext : DbContext
    {
        public EasyTasksContext(DbContextOptions<EasyTasksContext> options)
            :base(options)
        {

        }

        public DbSet<AppTask> Tasks { get; set; }
        public DbSet<AppSupportUser> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new AppUserConfig());
            modelBuilder.ApplyConfiguration(new AppTaskConfig());
            
        }
    }
}
