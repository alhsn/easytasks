﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Interfaces;
using Core.Entities;
using Infra.Repositories;
using Infra.Identity;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Threading.Tasks;

namespace Infra.Data
{
    public class AppUnitOfWork : IAppUnitOfWork
    {
        private readonly EasyTasksContext _appContext;
        private readonly AppIdentityContext _identityContext;

        public IAppSupportUserRepository UsersRepository { get; private set; }
        public IAppTaskRepository TasksRepository { get; private set; }

        public AppUnitOfWork(EasyTasksContext appContext,
            AppIdentityContext identityContext,
            RoleManager<IdentityRole> roleManager,
            UserManager<ApplicationUser> userManager)
        {
            _appContext = appContext;
            _identityContext = identityContext;
            UsersRepository = new AppSupportUserRepository(_appContext);
            TasksRepository = new AppTaskRepository(_appContext);
            
            // if no support users exists, run seed function
            if (!UsersRepository.Any())
            {
                //Clean up all identity users
                _identityContext.Users.RemoveRange(_identityContext.Users.ToList());
                // Then call the seed function
                EasyTasksSeed.SeedUsersAndTasks(this, roleManager, userManager);
            }

            // need to save seed identity data since they are not part of any repository
            _identityContext.SaveChanges(); 
        }

        public void Save()
        {
            _appContext.SaveChanges();
        }
    }
}
