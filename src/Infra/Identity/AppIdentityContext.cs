﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Infra.Identity
{
    public class AppIdentityContext : IdentityDbContext<ApplicationUser>
    {
        public AppIdentityContext(DbContextOptions<AppIdentityContext> options)
            :base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        //public static async Task SeedIdentityRoleAsync(RoleManager<IdentityRole> roleManager)
        //{
        //    if(!roleManager.RoleExistsAsync("Admin").Result)
        //    {
        //        await roleManager.CreateAsync(new IdentityRole("Admin"));
        //    }

        //    if (!roleManager.RoleExistsAsync("Support").Result)
        //    {
        //        await roleManager.CreateAsync(new IdentityRole("Support"));
        //    }
        //}

        //public static async Task SeedIdentitiesAsync(UserManager<ApplicationUser> userManager)
        //{
        //    if(userManager.FindByEmailAsync("Support_User_1@example.com").Result == null)
        //    {
        //        var SupportUser1 = new ApplicationUser { UserName = "Support_User_1", Email = "Support_User_1@example.com", AppId = 1 };
        //        var SupportUser2 = new ApplicationUser { UserName = "Support_User_2", Email = "Support_User_2@example.com", AppId = 2 };
        //        var SupportUser3 = new ApplicationUser { UserName = "Support_User_3", Email = "Support_User_3@example.com", AppId = 3 };
        //        var SupportUser4 = new ApplicationUser { UserName = "Support_User_4", Email = "Support_User_4@example.com", AppId = 4 };

        //        var AdminUser1 = new ApplicationUser { UserName = "Admin_User_1", Email = "Admin_User_1@example.com" };
        //        var AdminUser2 = new ApplicationUser { UserName = "Admin_User_2", Email = "Admin_User_2@example.com" };
            
        //        await userManager.CreateAsync(SupportUser1, "Support#1");
        //        await userManager.CreateAsync(SupportUser2, "Support#2");
        //        await userManager.CreateAsync(SupportUser3, "Support#3");
        //        await userManager.CreateAsync(SupportUser4, "Support#4");

        //        await userManager.CreateAsync(AdminUser1, "Admin@111");
        //        await userManager.CreateAsync(AdminUser1, "Admin@222");

        //        await userManager.AddToRoleAsync(SupportUser1, "Support");
        //        await userManager.AddToRoleAsync(SupportUser2, "Support");
        //        await userManager.AddToRoleAsync(SupportUser3, "Support");
        //        await userManager.AddToRoleAsync(SupportUser4, "Support");

        //        await userManager.AddToRoleAsync(AdminUser1, "Admin");
        //        await userManager.AddToRoleAsync(AdminUser2, "Admin");

        //    }
        //}
    }
}
