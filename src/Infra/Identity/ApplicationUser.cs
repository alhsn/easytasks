﻿using Microsoft.AspNetCore.Identity;

namespace Infra.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public int AppId { get; set; }

    }
}
