﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Core.Interfaces;
using Core.Entities;
using Core.Services;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Security;
using Web.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Infra.Identity;

namespace Web.Controllers
{
    [Authorize(Roles = "Support")]
    public class EasyTasksController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAppTaskService _taskService;
        public EasyTasksController(IAppTaskService taskService, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _taskService = taskService;
        }

        private int GetUserId()
        {
            return _userManager.GetUserAsync(HttpContext.User).Result.AppId;
        }

        public IActionResult Index()
        {
            int userid = GetUserId();
            var own_tasks = _taskService.GetOwnTasks(userid);
            var created_tasks = _taskService.GetCreatedTasks(userid);
            var assigned_tasks = _taskService.GetAssignedTasks(userid);

            ViewData["UserId"] = GetUserId();

            ViewData["Own"] = own_tasks;
            ViewData["Own_Count"] = own_tasks.Count();
            ViewData["Own_Done"] = own_tasks.Where(t => t.Completed).Count();

            ViewData["Created"] = created_tasks;
            ViewData["Created_Count"] = created_tasks.Count();
            ViewData["Created_Done"] = created_tasks.Where(t => t.Completed).Count();

            ViewData["Assigned"] = assigned_tasks;
            ViewData["Assigned_Count"] = assigned_tasks.Count();
            ViewData["Assigned_Done"] = assigned_tasks.Where(t => t.Completed).Count();


            return View();
        }

        public IActionResult ViewTask(int id)
        {
            var userId = GetUserId();
            var task = _taskService.GetTask(id, userId);
            if(task != null)
            {
                var canMark = task.CanMarkComplete(userId);
                var canDelete = task.CanDelete(userId);
                var own = task.IsOwnTask(userId);

                ViewData["Type"] = own ? "ToDo Item" : (canDelete ?"Created":"Assigned" ) +" Task";
                ViewBag.canDelete = canDelete;
                ViewBag.canMark = canMark;
                ViewBag.own = own;

                return View(TasksViewModel.FromAppTask(task));
            }
            return NotFound();
        }

        public IActionResult CreateToDo()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateToDo([Bind(include: "Title,Details")] TasksViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = GetUserId();
                _taskService.CreateTask(model.Title, model.Details, userId, userId);
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        public IActionResult CreateTask()
        {
            var other_users = _taskService.GetAllOtherUsers(GetUserId());
            ViewData["Others"] = new SelectList(other_users, "Id", "UserName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateTask([Bind(include: "Title,Details,AssignedToId")] TasksViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = GetUserId();
                _taskService.CreateTask(model.Title, model.Details, userId, model.AssignedToId);
                return RedirectToAction(nameof(Index));
            }

            var other_users = _taskService.GetAllOtherUsers(GetUserId());
            ViewData["Others"] = new SelectList(other_users, "Id", "UserName");
            return View(model);
        }


        public IActionResult MarkAsComplete(int id)
        {
            var userId = GetUserId();
            var task = _taskService.GetTask(id, userId);
            if (task != null)
            {
                var canMark = task.CanMarkComplete(userId);

                if (canMark)
                {
                    _taskService.MarkAsComplete(task.Id);
                    return RedirectToAction(nameof(ViewTask), new { id = task.Id });
                }
                else
                {
                    return BadRequest();
                }
            }
            return NotFound();
        }

        public IActionResult Delete(int id)
        {
            var userId = GetUserId();
            var task = _taskService.GetTask(id, userId);
            if (task != null)
            {
                var canDelete = task.CanDelete(userId);
                if (canDelete)
                {
                    _taskService.DeleteTask(id);
                    return RedirectToAction(nameof(Index));
                }
            }
            return BadRequest();
        }
    }
}