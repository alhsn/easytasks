﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Interfaces;
using Infra.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IAppTaskService _taskService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IAppTaskService taskService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _taskService = taskService;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string ReturnUrl = "/")
        {
            _signInManager.SignOutAsync().Wait();
            //prepare Demo logins
            var admins = _userManager.GetUsersInRoleAsync("Admin").Result;
            var users = _userManager.GetUsersInRoleAsync("Support").Result;
            
            ViewData["Admins"] = admins.OrderBy(a => a.UserName);
            ViewData["Users"] = users.OrderBy(u => u.UserName);
            ViewData["ReturnUrl"] = ReturnUrl;
            return View();
        }

        [HttpPost]
        [ActionName("Login")]
        [AllowAnonymous]
        public async Task<IActionResult> Signin(string userEmail, string ReturnUrl = "/")
        {
            var user = _userManager.FindByEmailAsync(userEmail).Result;
            if(user != null)
            {
                await _signInManager.SignInAsync(user, false);
                if (_userManager.IsInRoleAsync(user, "Support").Result)
                {
                    return RedirectToAction("Index", "EasyTasks");
                }
                return RedirectToAction("Index", "Admin");
            }
            return RedirectToAction(nameof(Login));
        }

        public  IActionResult Logout()
        {
            _signInManager.SignOutAsync().Wait();
            return Redirect(Url.Content("~/"));
        }

        [AllowAnonymous]
        public IActionResult AccessDenied()
        {
            return View();
        }

    }
}