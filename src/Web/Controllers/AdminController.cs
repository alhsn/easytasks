﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Core.Interfaces;
using Core.Entities;
using Core.Services;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Security;
using Web.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Infra.Identity;
using Newtonsoft.Json;

namespace Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAppTaskService _taskService;
        public AdminController(IAppTaskService taskService, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _taskService = taskService;
        }

        private int GetUserId()
        {
            return _userManager.GetUserAsync(HttpContext.User).Result.AppId;
        }


        public IActionResult Index()
        {
            var tasks = _taskService.GetAllTasks();
            var taskModels = new List<TasksViewModel>();
            taskModels.AddRange(tasks.Select(t=> TasksViewModel.FromAppTask(t)));
            return View(taskModels);
        }

        public IActionResult AllTaskJson()
        {
            var tasks = _taskService.GetAllTasks();
            return Content(JsonConvert.SerializeObject(tasks), "application/json");
        }

        public IActionResult Details(int id)
        {
            var task = _taskService.GetTask(id);
            if (task != null)
            {
                return View(TasksViewModel.FromAppTask(task));
            }
            return NotFound();
        }

        public IActionResult Delete(int id)
        {
            var task = _taskService.GetTask(id);
            if (task != null)
            {
                _taskService.DeleteTask(task.Id);
                return RedirectToAction(nameof(Index));
            }
            return NotFound();
        }
    }
}