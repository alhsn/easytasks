﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Core.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Web.ViewModels
{
    public class TasksViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }
        [Required]
        public string Details { get; set; }
        [Required]
        public bool Completed { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int CreatedById { get; set; }
        [Editable(false)]
        public string CreatedBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int AssignedToId { get; set; }
        [Editable(false)]
        public string AssignedTo { get; set; }

        public static TasksViewModel FromAppTask(AppTask appTask)
        {
            return new TasksViewModel
            {
                Id = appTask.Id,
                Title = appTask.Title,
                Details = appTask.Details,
                Completed = appTask.Completed,
                CreatedBy = appTask.CreatedBy?.UserName,
                CreatedById = appTask.CreatedById,
                AssignedTo = appTask.AssignedTo?.UserName,
                AssignedToId = appTask.AssignedToId,
            };
        }
    }
}
