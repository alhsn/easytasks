﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Core.Interfaces;
using Core.Entities;

namespace Core.Services
{
    public class AppTaskService : IAppTaskService
    {
        private readonly IAppUnitOfWork _unitOfWork;

        public AppTaskService(IAppUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void CreateTask(string title, string details, int creatorId, int assigneeId)
        {
            var task = new AppTask() {
                Title = title, Details = details,
                AssignedToId = assigneeId, CreatedById = creatorId
            };

            _unitOfWork.TasksRepository.Add(task);
            _unitOfWork.Save();
        }

        public void DeleteTask(int taskId)
        {
            var task = _unitOfWork.TasksRepository.Get(taskId);
            if(task != null)
            {
                _unitOfWork.TasksRepository.Remove(task);
            }
            _unitOfWork.Save();
        }

        public IEnumerable<AppSupportUser> GetAllOtherUsers(int userId)
        {
            return _unitOfWork.UsersRepository.AllWhere(u => u.Id != userId);
        }

        public IEnumerable<AppTask> GetAllTasks()
        {
            return _unitOfWork.TasksRepository.GetAppTasksWithUsers().Result;
        }

        public IEnumerable<AppSupportUser> GetAllUsers()
        {
            return _unitOfWork.UsersRepository.AllWhere(a => true);
        }

        public IEnumerable<AppTask> GetAssignedTasks(int assigneeId)
        {
            return _unitOfWork.TasksRepository
                .AllWhere(t => t.AssignedToId == assigneeId && t.CreatedById != assigneeId);
        }

        public IEnumerable<AppTask> GetCreatedTasks(int creatorId)
        {
            return _unitOfWork.TasksRepository.AllWhere(t => t.AssignedToId != creatorId && t.CreatedById == creatorId);
        }

        public IEnumerable<AppTask> GetOwnTasks(int userId)
        {
            return _unitOfWork.TasksRepository.AllWhere(t => t.AssignedToId == userId && t.CreatedById == userId);
        }

        public AppTask GetTask(int taskId)
        {
            return _unitOfWork.TasksRepository.GetAppTaskWithUsers(taskId);
        }

        public AppTask GetTask(int taskId, int userId)
        {
            return _unitOfWork.TasksRepository.GetAppTaskWithUsers(taskId, userId);
        }

        public void MarkAsComplete(int taskId)
        {
            var task = _unitOfWork.TasksRepository.Get(taskId);
            task.Completed = true;
            _unitOfWork.TasksRepository.Update(task);
            _unitOfWork.Save();
        }

        public void UpdateTask(AppTask task)
        {
            _unitOfWork.TasksRepository.Update(task);
            _unitOfWork.Save();
        }

        public void UpdateTask(AppTask task, int userId)
        {
            throw new NotImplementedException();
        }
    }
}
