﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class AppTask : AppEntity
    {
        public string Title { get; set; }
        public string Details { get; set; }
        public bool Completed { get; set; } = false;

        public int CreatedById { get; set; }
        public virtual AppSupportUser CreatedBy { get; set; }

        public int AssignedToId { get; set; }
        public virtual AppSupportUser AssignedTo { get; set; }

        public void MarkCompleted()
        {
            Completed = true;
        }

        public bool IsOwnTask(int userId)
        {
            return CreatedById == AssignedToId && CreatedById == userId;
        }

        public bool CanMarkComplete(int userId)
        {
            return userId == AssignedToId && !Completed;
        }

        public bool CanDelete(int userId)
        {
            return userId == CreatedById;
        }
    }
}
