﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Interfaces
{
    public interface IAppTaskRepository : IAppRepository<AppTask>
    {
        Task<IEnumerable<AppTask>> GetAppTasksWithUsers();
        AppTask GetAppTaskWithUsers(int taskId);
        AppTask GetAppTaskWithUsers(int taskId, int userId);
    }
}
