﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Interfaces
{
    public interface IAppRepository<T> where T: AppEntity
    {
        T Get(int id);
        Task<IEnumerable<T>> GetAllAsync();

        IEnumerable<T> AllWhere(Expression<Func<T, bool>> expression);
        T SingleOrDefault(Expression<Func<T, bool>> expression);

        bool Any();
        
        void Add(T entity);
        void AddRange(ICollection<T> entities);

        void Remove(T entity);
        void RemoveRange(ICollection<T> entities);

        void Update(T entity);
    }
}
