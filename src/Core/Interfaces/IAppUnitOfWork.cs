﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Interfaces
{
    public interface IAppUnitOfWork
    {
        IAppSupportUserRepository UsersRepository { get; }
        IAppTaskRepository TasksRepository { get; }
        void Save();
    }
}
