﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Interfaces
{
    public interface IAppTaskService
    {
        AppTask GetTask(int taskId);
        AppTask GetTask(int taskId, int userId);
        IEnumerable<AppTask> GetAllTasks();
        IEnumerable<AppTask> GetOwnTasks(int userId);
        IEnumerable<AppTask> GetCreatedTasks(int creatorId);
        IEnumerable<AppTask> GetAssignedTasks(int assigneeId);
        void CreateTask(string title, string details, int creatorId, int assigneeId);
        void DeleteTask(int taskId);
        void UpdateTask(AppTask task);
        void UpdateTask(AppTask task, int userId);
        void MarkAsComplete(int taskId);

        IEnumerable<AppSupportUser> GetAllUsers();
        IEnumerable<AppSupportUser> GetAllOtherUsers(int userId);
    }
}
