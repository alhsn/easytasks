﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Interfaces
{
    public interface IAppSupportUserRepository : IAppRepository<AppSupportUser>
    {
        Task<AppSupportUser> GetSupportUserWithAssignedTasks(int id);
        Task<AppSupportUser> GetSupportUserWithCreatedTasks(int id);
    }
}
